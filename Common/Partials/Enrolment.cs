﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Common.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Common.Models
{
    [ModelMetadataType(typeof(EnrolmentMetadata))]
    public partial class Enrolment
    {
    }

    public class EnrolmentMetadata
    {
        [Required]
        [DisplayName("Student")]
        public int StudentId { get; set; }

        [Required]
        [DisplayName("Course")]
        public int CourseId { get; set; }

        [Required]
        [StartDate(ErrorMessage = "Start Date should be a Monday")]
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }

        [Required]
        [EndDate (ErrorMessage = "End Date should be a Friday")]
        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]        
        public DateTime EndDate { get; set; }
    }
}
