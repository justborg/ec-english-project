﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Common.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Common.Models
{
    [ModelMetadataType(typeof(HolidayMetadata))]
    public partial class Holiday
    {
    }

    public class HolidayMetadata
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DisplayName("Student")]
        public int StudentId { get; set; }

        [Required]
        [StartDate(ErrorMessage = "Start Date should be a Monday")]
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }

        [Required]
        [EndDate(ErrorMessage = "End Date should be a Friday")]
        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime EndDate { get; set; }
    }
}
