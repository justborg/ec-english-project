﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Common.Models
{
    [ModelMetadataType(typeof(CourseMetadata))]
    public partial class Course
    {
    }

    public class CourseMetadata
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Range(1, int.MaxValue)]
        [DisplayName("Duration (in weeks)")]
        public int NoOfWeeks { get; set; }
    }
}
