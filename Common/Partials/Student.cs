﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Models
{
    [ModelMetadataType(typeof(StudentMetadata))]
    public partial class Student
    {
        [ReadOnly(true)]
        public string SearchKeywords
        {
            get
            {
                return Name + " " + Surname + " " + Email;
            }
        }
    }

    public class StudentMetadata
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string Surname { get; set; }

        [Required]
        [StringLength(500)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }        
    }
}
