﻿
namespace Common.CustomModels
{
    public class ScheduleStateModel
    {
        public enum ObjectState { None, Add, Update };
        public ObjectState State { get; set; } = ObjectState.None;
        public dynamic Object { get; set; }
    }

}
