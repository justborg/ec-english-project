﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Common.Utils
{
    public static class DateTimeExtension
    {
        public static int GetWeekOfYear(this DateTime date)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            return cultureInfo.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static DateTime AddWeeks(this DateTime date, int noOfWeeks)
        {
            return date.AddDays(noOfWeeks * 7);
        }

        public static DateTime GetPrevious(this DateTime date, DayOfWeek dayOfWeek)
        {
            while (date.DayOfWeek != dayOfWeek)
                date = date.AddDays(-1);

            return date;
        }

        public static DateTime GetNext(this DateTime date, DayOfWeek dayOfWeek)
        {
            while (date.DayOfWeek != dayOfWeek)
                date = date.AddDays(1);

            return date;
        }
    }
}
