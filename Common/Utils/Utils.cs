﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Common.Utils
{
    public static class ExpressionHandler<TObject>
    {
        public static Expression<Func<TObject, object>>[] Include(params Expression<Func<TObject, object>>[] includeExpressions)
        {
            return includeExpressions;
        }
    }

    public static class Utils
    {
        public static dynamic ConvertSuperClassToSubClass<T>(dynamic item)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(item));
        }
    }
}
