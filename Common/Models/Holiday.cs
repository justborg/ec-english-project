﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public partial class Holiday
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Student Student { get; set; }
    }
}
