﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public partial class Course
    {
        public Course()
        {
            Enrolment = new HashSet<Enrolment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NoOfWeeks { get; set; }

        public virtual ICollection<Enrolment> Enrolment { get; set; }
    }
}
