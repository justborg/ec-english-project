﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public partial class Student
    {
        public Student()
        {
            Enrolment = new HashSet<Enrolment>();
            Holiday = new HashSet<Holiday>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Enrolment> Enrolment { get; set; }
        public virtual ICollection<Holiday> Holiday { get; set; }
    }
}
