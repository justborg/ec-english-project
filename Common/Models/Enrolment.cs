﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public partial class Enrolment
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Split { get; set; }
        public Guid SplitGroupId { get; set; }
        public int Part { get; set; }

        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }
    }
}
