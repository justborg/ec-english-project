﻿using Common.ContextHelpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public partial class ECEnglishContext : IContext
    {
        public DbContext GetContext() { return this; }
    }
}
