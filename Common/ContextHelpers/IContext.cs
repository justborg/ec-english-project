﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.ContextHelpers
{
    public interface IContext
    {
        DbContext GetContext();
    }
}
