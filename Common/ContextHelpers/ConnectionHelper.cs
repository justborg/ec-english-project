﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.ContextHelpers
{
    public class ConnectionHelper
    {
        public static string ECEnglishConnectionString { get; private set; }

        public static void InitConnectionString(IConfiguration configuration)
        {
            ECEnglishConnectionString = configuration.GetConnectionString("ECEnglishConnection");
        }
    }
}
