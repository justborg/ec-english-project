﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess
{
    public abstract class BaseActions<TObject> where TObject : class
    {
        #region Save
        protected static async Task<TObject> ExecuteSaveAsync(DbContext context, TObject t)
        {
            context.Set<TObject>().Add(t);
            await context.SaveChangesAsync();
            return t;
        }

        protected static async Task<object> ExecuteSaveRangeAsync(DbContext context, List<TObject> t)
        {
            context.AddRange(t);
            return await context.SaveChangesAsync();
        }
        #endregion

        #region Update
        protected static async Task<TObject> ExecuteUpdateAsync(DbContext context, TObject t, object key)
        {
            if (t == null)
                return null;

            var objectExists = await context.Set<TObject>().FindAsync(key);
            if (objectExists != null)
            {
                context.Entry(objectExists).CurrentValues.SetValues(t);
                await context.SaveChangesAsync();
            }

            return objectExists;
        }

        protected static async Task<object> ExecuteUpdateRangeAsync(DbContext context, List<TObject> t)
        {
            if (t == null)
                return null;

            context.UpdateRange(t);
            return await context.SaveChangesAsync();
        }
        #endregion

        #region Delete
        protected static string ExecuteDelete(DbContext context, TObject t)
        {
            try
            {
                context.Set<TObject>().Remove(t);
                return context.SaveChanges() > 0 ? "" : "No items deleted";
            }
            catch (DbUpdateException dbUpdateException)
            {
                string errMsg = dbUpdateException.InnerException.Message;
                return (errMsg.ToLower().Contains("the delete statement conflicted with reference constraint"))
                    ? "Item cannot be deleted since it has associated dependencies"
                    : errMsg;
            }
        }
        #endregion

        #region Get
        protected static async Task<TObject> ExecuteGetAsync(DbContext context, object id)
        {
            return await context.Set<TObject>().FindAsync(id);
        }

        protected static async Task<List<TObject>> ExecuteGetAllAsync(DbContext context, params Expression<Func<TObject, object>>[] includes)
        {
            var result = context.Set<TObject>().AsQueryable();
            return await includes.Aggregate(result, (current, include) => current.Include(include)).ToListAsync();
        }

        protected static async Task<List<TObject>> ExecuteGetAllAsync(DbContext context, Expression<Func<TObject, bool>> whereClause, params Expression<Func<TObject, object>>[] includes)
        {
            var result = context.Set<TObject>().AsQueryable();
            return await includes.Aggregate(result, (current, include) => current.Include(include)).Where(whereClause).AsQueryable().ToListAsync();
        }

        //protected static async Task<List<TSelect>> ExecuteGetAllAsync<TSelect>(DbContext context, Expression<Func<TObject, bool>> whereClause, Expression<Func<TObject, TSelect>> selectClause, params Expression<Func<TObject, object>>[] includes)
        //{
        //    return await context.Set<TObject>().Where(whereClause).Select(selectClause).AsQueryable().ToListAsync();
        //}
        #endregion
    }
}
