﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Common.ContextHelpers;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class ContextBaseActions<TObject, TContext> : BaseActions<TObject>
        where TObject : class
        where TContext : IContext
    {
        private static DbContext GetContext()
        {
            return ((TContext)Activator.CreateInstance(typeof(TContext))).GetContext();
        }

        #region Save
        public static async Task<TObject> SaveAsync(TObject t)
        {
            using (var context = GetContext())
            {
                return await ExecuteSaveAsync(context, t);
            }
        }

        public static async Task<object> SaveRangeAsync(List<TObject> t)
        {
            using (var context = GetContext())
            {
                return await ExecuteSaveRangeAsync(context, t);
            }
        }
        #endregion

        #region Update
        public static async Task<TObject> UpdateAsync(TObject t, object key)
        {
            using (var context = GetContext())
            {
                return await ExecuteUpdateAsync(context, t, key);
            }
        }

        public static async Task<object> UpdateRangeAsync(List<TObject> t)
        {
            using (var context = GetContext())
            {
                return await ExecuteUpdateRangeAsync(context, t);
            }
        }
        #endregion

        #region Delete
        public static string Delete(TObject t)
        {
            using (var context = GetContext())
            {
                return ExecuteDelete(context, t);
            }
        }

        public static string Delete(object id)
        {
            using (var context = GetContext())
            {
                return ExecuteDelete(context, ExecuteGetAsync(context, id).Result);
            }
        }
        #endregion

        #region Get
        public static async Task<TObject> GetAsync(object id)
        {
            using (var context = GetContext())
            {
                return await ExecuteGetAsync(context, id);
            }
        }

        public static async Task<List<TObject>> GetAllAsync(params Expression<Func<TObject, object>>[] includes)
        {
            using (var context = GetContext())
            {
                return await ExecuteGetAllAsync(context, includes);
            }
        }

        public static async Task<List<TObject>> GetAllAsync(Expression<Func<TObject, bool>> whereClause, params Expression<Func<TObject, object>>[] includes)
        {
            using (var context = GetContext())
            {
                return await ExecuteGetAllAsync(context, whereClause, includes);
            }
        }
        #endregion
    }
}
