﻿using BusinessLogic.Helpers;
using Common.CustomModels;
using Common.Models;
using Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Utils
{
    public class ScheduleHelper
    {
        /// <summary>
        /// Checks the schedule of the given student for any conflicts
        /// </summary>
        /// <param name="studentId">Id of the student to check schedule of</param>
        /// <param name="addedObject">The last added object to the schedule</param>
        public static async void CheckSchedule(int studentId, dynamic addedObject)
        {
            try
            {
                DateTime start = addedObject.StartDate, end = addedObject.EndDate;

                //Get all possible conflicting holidays and enrolments
                var holidays = HolidayBL.GetAllAsync(x => x.StudentId == studentId && ((x.StartDate <= start && x.EndDate > start) || (x.StartDate >= start))).Result;
                var enrolments = EnrolmentBL.GetAllAsync(x => x.StudentId == studentId && ((x.StartDate <= start && x.EndDate > start) || (x.StartDate >= start))).Result;

                if (enrolments.Count > 0 || holidays.Count > 0)
                {
                    var schedule = new List<ScheduleStateModel>();
                    schedule.AddRange(holidays.Select(x => new ScheduleStateModel { Object = x }));
                    schedule.AddRange(enrolments.Select(x => new ScheduleStateModel { Object = x }));

                    //if there 2 or more conflicts in schedule are fournd.. run SolveScheduelConflicts
                    if (schedule.Count >= 2)
                    {
                        //run recursion method to solve conflicts
                        schedule = SolveScheduelConflicts(schedule);

                        //Save any new enrolments that emerged from splitting enrolments while solving conflicts in schedule
                        //get all enrolments with State "Add" cast them to Enrolment and Save them to DB
                        var enrolmentsToSave = schedule.Where(x => x.State == ScheduleStateModel.ObjectState.Add && x.Object.GetType().Name.Equals(nameof(Enrolment))).ToList();
                        if (enrolmentsToSave.Count() > 0)
                        {
                            var toSave = enrolmentsToSave.Select(x => x.Object).Cast<Enrolment>().ToList();
                            await EnrolmentBL.SaveRangeAsync(toSave);
                        }

                        //Update any enrolments that has been updated while running the solve conflicts method
                        //get all enrolments with State "Update" cast them to Enrolment and Update DB
                        var enrolmentsToUpdate = schedule.Where(x => x.State == ScheduleStateModel.ObjectState.Update && x.Object.GetType().Name.Equals(nameof(Enrolment))).ToList();
                        if (enrolmentsToUpdate.Count() > 0)
                        {
                            var toUpdate = enrolmentsToUpdate.Select(x => x.Object).Cast<Enrolment>().ToList();
                            await EnrolmentBL.UpdateRangeAsync(toUpdate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Solves conflicts within the given list of schedules.
        /// </summary>
        /// <param name="schedule">List of student schedule</param>
        /// <param name="index">Current list index... leave it empty to start from the begining</param>
        /// <returns></returns>
        public static List<ScheduleStateModel> SolveScheduelConflicts(List<ScheduleStateModel> schedule, int index = 0)
        {
            //order the schedule by StartDate ASC and then by Type (1st Holiday or 2nd Enrolment)
            schedule = schedule.OrderBy(x => x.Object.StartDate).ThenByDescending(x => x.Object.GetType().Name).ToList();

            var newSchedule = schedule;
            var currentIndex = index;
            var nextIndex = index + 1;

            while (nextIndex < schedule.Count)
            {
                //get current and next item in list
                var currentItem = schedule[currentIndex].Object;
                var nextItem = schedule[nextIndex].Object;

                if (nextItem.StartDate <= currentItem.EndDate)
                {
                    //if next item is holiday and it conflicts with current item in schedule then split enrolments else move enrolment
                    if (nextItem.GetType().Name == nameof(Holiday))
                    {
                        //SPLIT
                        var currentItemEndDate = currentItem.EndDate;

                        //update End Date of current enrolment
                        currentItem.EndDate = ((DateTime)nextItem.StartDate).GetPrevious(DayOfWeek.Friday);

                        //create new enrolment from the current one and adjust the Dates
                        var schedulePart2 = new Enrolment()
                        {
                            StudentId = currentItem.StudentId,
                            CourseId = currentItem.CourseId,
                            StartDate = ((DateTime)currentItem.EndDate).GetNext(DayOfWeek.Monday),
                            EndDate = currentItemEndDate,
                            Split = true,
                            SplitGroupId = currentItem.SplitGroupId,
                            Part = currentItem.Part + 1
                        };

                        //add the newly created enrolment to the list of schedule
                        schedule.Add(new ScheduleStateModel() { Object = schedulePart2, State = ScheduleStateModel.ObjectState.Add });

                        //mark current item as split and update the state
                        currentItem.Split = true;
                        schedule[currentIndex].State = ScheduleStateModel.ObjectState.Update;
                    }
                    else
                    {
                        //MOVE
                        var diffStart = ((DateTime)nextItem.StartDate).GetWeekOfYear();
                        var diffEnd = ((DateTime)currentItem.EndDate).GetWeekOfYear();

                        //calcualte difference between dates
                        var totalWeekDifference = (diffEnd - diffStart) + 1;

                        nextItem.StartDate = ((DateTime)nextItem.StartDate).AddWeeks(totalWeekDifference);
                        nextItem.EndDate = ((DateTime)nextItem.EndDate).AddWeeks(totalWeekDifference);

                        if (schedule[nextIndex].State != ScheduleStateModel.ObjectState.Add)
                            schedule[nextIndex].State = ScheduleStateModel.ObjectState.Update;
                    }

                    index++;
                    return SolveScheduelConflicts(schedule, index);
                }
                else
                    break;
            }

            return newSchedule;
        }
    }
}
