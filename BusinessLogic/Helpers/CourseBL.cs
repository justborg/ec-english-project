﻿using Common.Models;
using Common.Utils;
using DataAccess;
using System;

namespace BusinessLogic.Helpers
{
    public class CourseBL : ContextBaseActions<Course, ECEnglishContext>
    {
        /// <summary>
        /// Gives back the end date of a given course based on the specified start date
        /// </summary>
        /// <param name="courseId">The course you need to get the end date of</param>
        /// <param name="startDate">The date you want the course to start</param>
        /// <returns></returns>
        public static DateTime GetCourseEndDate(int courseId, DateTime startDate)
        {
            //get course number of weeks and calculates the end date from the given start date
            var noOfWeeks = GetAsync(courseId).Result.NoOfWeeks;
            return startDate.AddWeeks(noOfWeeks).GetPrevious(DayOfWeek.Friday);
        }
    }
}
