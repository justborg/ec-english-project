﻿using Common.Models;
using DataAccess;
using System.Threading.Tasks;

namespace BusinessLogic.Helpers
{
    public class HolidayBL : ContextBaseActions<Holiday, ECEnglishContext>
    {
        /// <summary>
        /// Check if the specified holidays conflicts with an existing holiday
        /// </summary>
        /// <param name="holiday">The new holiday to check against</param>
        /// <returns></returns>
        public static bool HasConflictingStudentHolidays(Holiday holiday)
        {
            var holidays = GetAllAsync(x => x.StudentId == holiday.StudentId && !(holiday.EndDate <= x.StartDate || holiday.StartDate >= x.EndDate)).Result;
            return holidays.Count > 0;
        }
    }
}
