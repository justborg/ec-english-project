﻿using BusinessLogic.Helpers;
using Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EC_English.Controllers
{
    public class CoursesController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await CourseBL.GetAllAsync());
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var course = await CourseBL.GetAsync(id);
            if (course == null)
                return NotFound();

            return View(course);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Course course)
        {
            if(ModelState.IsValid)
            {
                course = await CourseBL.SaveAsync(course);
                return RedirectToAction(nameof(Index));
            }

            return View(course);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var course = await CourseBL.GetAsync(id);
            if (course == null)
                return NotFound();

            return View(course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Course course)
        {
            if (id != course.Id)
                return NotFound();

            if(ModelState.IsValid)
            {
                course = await CourseBL.UpdateAsync(course, id);

                return RedirectToAction(nameof(Index));
            }
            return View(course);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var course = await CourseBL.GetAsync(id);

            if (course == null)
                return NotFound();

            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            CourseBL.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
