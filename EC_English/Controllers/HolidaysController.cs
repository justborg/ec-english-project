﻿using BusinessLogic.Helpers;
using BusinessLogic.Utils;
using Common.Models;
using Common.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EC_English.Controllers
{
    public class HolidaysController : Controller
    {
        private void PopulateDataForCreate()
        {
            //Get list of students to populate the dropdown and add it to ViewBag
            ViewBag.Students = StudentBL.GetAllAsync().Result.OrderBy(x => x.Name).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name + " " + x.Surname });
        }

        [HttpGet]
        public IActionResult Create(int? studentId)
        {
            PopulateDataForCreate();

            if (studentId.HasValue)
            {
                var holiday = new Holiday()
                {
                    StudentId = studentId.Value
                };

                return View(holiday);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Holiday holiday)
        {
            if (ModelState.IsValid)
            {
                if (holiday.EndDate < holiday.StartDate)
                {
                    ViewBag.Message = "Holiday end date cannot be before holiday start date.";
                }
                else if (HolidayBL.HasConflictingStudentHolidays(holiday))
                {
                    ViewBag.Message = "Holiday dates conflict with a booked holiday.";
                }
                else
                {
                    holiday = await HolidayBL.SaveAsync(holiday);

                    //checks schedule for any conflicts
                    ScheduleHelper.CheckSchedule(holiday.StudentId, holiday);

                    return RedirectToAction("Details", "Students", new { id = holiday.StudentId });
                }
            }

            PopulateDataForCreate();
            return View(holiday);
        }

        [HttpGet]
        public JsonResult GetNextFriday(DateTime? date)
        {
            //if given date has a value return the next Friday from the specified date
            var result = (date.HasValue) ? date.Value.GetNext(DayOfWeek.Friday).ToString("yyyy-MM-dd") : string.Empty;
            return Json(result);
        }
    }
}
