﻿using BusinessLogic.Helpers;
using BusinessLogic.Utils;
using Common.Models;
using Common.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EC_English.Controllers
{
    public class EnrolmentsController : Controller
    {
        private void PopulateDataForCreate()
        {
            //Get list of students and courses to populate the dropdowns and add them to ViewBag
            ViewBag.Students = StudentBL.GetAllAsync().Result.OrderBy(x => x.Name).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name + " " + x.Surname });
            ViewBag.Courses = CourseBL.GetAllAsync().Result.OrderBy(x => x.Name).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });
        }

        [HttpGet]
        public IActionResult Create(int? studentId)
        {
            PopulateDataForCreate();

            if (studentId.HasValue)
            {
                var enrolment = new Enrolment()
                {
                    StudentId = studentId.Value
                };

                return View(enrolment);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Enrolment enrolment)
        {
            if (ModelState.IsValid)
            {
                if (enrolment.EndDate < enrolment.StartDate)
                {
                    ViewBag.Message = "Enrolment end date cannot be before holiday start date.";
                }
                else
                {
                    enrolment = await EnrolmentBL.SaveAsync(enrolment);

                    //check schedule for any conflicts
                    ScheduleHelper.CheckSchedule(enrolment.StudentId, enrolment);

                    return RedirectToAction("Details", "Students", new { id = enrolment.StudentId });
                }
            }

            PopulateDataForCreate();
            return View(enrolment);
        }

        [HttpGet]
        public JsonResult GetCourseEndDate(int? courseId, DateTime? startDate)
        {
            var result = string.Empty;

            if(courseId.HasValue && startDate.HasValue)
            {
                result = CourseBL.GetCourseEndDate(courseId.Value, startDate.Value).ToString("yyyy-MM-dd");
            }   

            return Json(result);
        }
    }
}
