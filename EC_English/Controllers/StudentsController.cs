﻿using BusinessLogic.Helpers;
using Common.CustomModels;
using Common.Models;
using Common.Utils;
using EC_English.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EC_English.Controllers
{
    public class StudentsController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Index(string search)
        {
            //if search keyword is specified filter the list of students else return all students
            var students = (String.IsNullOrWhiteSpace(search))
                    ? await StudentBL.GetAllAsync()
                    : await StudentBL.GetAllAsync(x => x.SearchKeywords.Contains(search, StringComparison.CurrentCultureIgnoreCase));

            return View(students);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (!id.HasValue)
                return NotFound();

            try
            {
                //get student and cast it to a StudentScheduleModel
                var student = StudentBL.GetAllAsync(x => x.Id == id).Result.FirstOrDefault();
                var studentSchedule = Utils.ConvertSuperClassToSubClass<StudentScheduleModel>(student);

                //get student current and future holidays and enrolments
                var enrolments = await EnrolmentBL.GetAllAsync(x => x.StudentId == id && x.StartDate >= DateTime.Today, ExpressionHandler<Enrolment>.Include(x => x.Course));
                var holidays = await HolidayBL.GetAllAsync(x => x.StudentId == id && x.StartDate >= DateTime.Today);

                //add the student holidays and enrolments to ScheduleModel
                var schedule = new List<ScheduleModel>();

                schedule.AddRange(holidays.Select((x) => new ScheduleModel
                {
                    Type = x.GetType().Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                }));

                schedule.AddRange(enrolments.Select((x) => new ScheduleModel
                {
                    Type = (x.Split) ? $"{x.Course.Name} (Part {x.Part})" : x.Course.Name,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate
                }));

                //order the list by StartDate ASC and then by Type DESC
                studentSchedule.Schedule = schedule.OrderBy(x => x.StartDate).ThenByDescending(x => x.Type).ToList();

                return View(studentSchedule);
            }
            catch (Exception ex)
            {
                //log error
                return View(new StudentScheduleModel());
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Student student)
        {
            if (ModelState.IsValid)
            {
                student = await StudentBL.SaveAsync(student);
                return RedirectToAction(nameof(Index));
            }

            return View(student);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var student = await StudentBL.GetAsync(id);

            if (student == null)
                return NotFound();

            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Student student)
        {
            if (id != student.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                student = await StudentBL.UpdateAsync(student, id);

                return RedirectToAction(nameof(Index));
            }

            return View(student);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var course = await StudentBL.GetAsync(id);

            if (course == null)
                return NotFound();

            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            StudentBL.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
